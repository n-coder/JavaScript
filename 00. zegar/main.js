function reload(){
   
    var today = new Date();

    var rok = today.getFullYear();
    var miesiac = today.getMonth();
    switch (miesiac){
        case 0: 
            miesiac = "Stycznia";
            break;
        case 1: 
            miesiac = "Lutego";
            break;
        case 2: 
            miesiac = "Marca";
            break;
        case 3: 
            miesiac = "Kwiatnia";
            break;
        case 4: 
            miesiac = "Maja";
            break;
        case 5: 
            miesiac = "Czerwca";
            break;
        case 6: 
            miesiac = "Lipca";
            break;
        case 7: 
            miesiac = "Sierpnia";
            break;
        case 8: 
            miesiac = "Września";
            break;
        case 9: 
            miesiac = "Października";
            break;
        case 10: 
            miesiac = "Listopada";
            break;
        case 11: 
            miesiac = "Grudnia";
            break;
        default:
            alert("wrong date");
    }
    var dzien = today.getDate();

    var godzina = today.getHours();
        if (godzina < 10) {
            godzina = "0" + godzina;
        }
    var minuta = today.getMinutes();
        if (minuta <10) {
            minuta = "0" + minuta;
        }
    var sekunda = today.getSeconds();
        if (sekunda < 10) {
            sekunda = "0" + sekunda;
        }
    
    var zegar = dzien + " " + miesiac + " " + rok + " | " + godzina + ":" + minuta +":" + sekunda;
    
    document.getElementById("zegar").innerHTML = zegar;
    
    setTimeout("reload()", 1000);
}