var numerSlidu = Math.floor(Math.random()*5)+1;

var timer1 = 0;
var timer2 = 0;

function ustaw(slidenr)
{
    clearTimeout(timer1);
    clearTimeout(timer2);
    numerSlidu = slidenr - 1;
    hide();
    setTimeout("slider()", 500);
}

function hide()
{
    $("#slider").fadeOut(500);
}

function slider()
{
    numerSlidu++;
    if (numerSlidu > 3)
    {
        numerSlidu = 1;
    }

    var file = "<img src=\"img/slide" + numerSlidu + ".png\" />";

    document.getElementById("slider").innerHTML = file;
    $("#slider").fadeIn(1000);

    timer1 = setTimeout("slider()", 5000);
    timer2 = setTimeout("hide()", 4500);
}