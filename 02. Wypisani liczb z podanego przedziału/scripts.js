function wypisz() {
    var liczba1 = document.getElementById("liczba1").value;
    var liczba2 = document.getElementById("liczba2").value;
    var napis = "";

    if (isNaN(liczba1)) 
    {
        document.getElementById("wynik").innerHTML = "Wpisz poprawną liczbę w polu 1";
    } 
    else if (isNaN(liczba2)) 
    {
        document.getElementById("wynik").innerHTML = "Wpisz poprawną liczbę w polu 2";
    } 
    else 
    { 
        if (liczba1 < liczba2) 
        {   
            for(i=liczba1; i <= liczba2; i++) {
                napis = napis + i + " ";
            }
        document.getElementById("wynik").innerHTML = napis;
        } 
        else if ( liczba2 < liczba1) 
        {
            for(i=liczba2; i <= liczba1; i++) 
            {
                napis = napis + i + " ";
            }
        document.getElementById("wynik").innerHTML = napis;
        } 
        else 
        {
            document.getElementById("wynik").innerHTML = "Nie podano wartości!";
        }
    }
}